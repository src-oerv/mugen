#!/usr/bin/bash

# Copyright (c) 2022. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   hua
# @Contact   :   dchang@zhixundn.com
# @Date      :   2022/11/20
# @License   :   Mulan PSL v2
# @Desc      :   verify the uasge of papi command
# ############################################

source ${OET_PATH}/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL papi
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."

    papi_xml_event_info -h 2>&1 | grep "Usage: papi_xml_event_info"
    CHECK_RESULT $? 0 0 "Check 'papi_xml_event_info -h failed"

    papi_xml_event_info -p | grep 'component index="0" type="CPU"'
    CHECK_RESULT $? 0 0 "Check 'papi_xml_event_info -p failed"

    papi_xml_event_info -n | grep 'component index="1" type="Unknown"'
    CHECK_RESULT $? 0 0 "Check 'papi_xml_event_info -n failed"

    papi_xml_event_info -c 1 | grep 'cpuMaxMegahertz value'
    CHECK_RESULT $? 0 0 "Check 'papi_xml_event_info -c failed"

    papi_version | grep "PAPI Version: [[:digit:]]"
    CHECK_RESULT $? 0 0 "Check 'papi_version failed"

    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    DNF_REMOVE
    LOG_INFO "End to restore the test environment."
}

main "$@"
